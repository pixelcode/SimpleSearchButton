$('#searchfield').keyup(function(){
	search();
});

$('#searchbuttondiv').click(function(){
	var searchfield = document.getElementById('searchfield').style.display;
	document.getElementById('searchfield').value = '';
	search();
	$('#searchfield').animate({ 'width': 'toggle' }, 300, 'swing');
	var html = $('#searchbutton').html();
    $('#searchbutton').html(
		html == '<i class="fas fa-search"></i>' ? '<a style="font-size: 25px"><i class="fas fa-times"></i></a>' : '<i class="fas fa-search"></i>');
});

if($('#searchfield').val() == ''){
	$('article').css({ 'display': 'block' });
}

function search(){
	var searchquery = document.getElementById('searchfield').value.toLowerCase();
	$('article').css({ 'display': 'block' });
	$('article').filter(function () {
		return ($(this).find('*').text().toLowerCase().indexOf(searchquery) == -1)
	}).css({ 'display': 'none' });
}