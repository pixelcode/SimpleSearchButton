# SimpleSearchButton

A simple floating search button extending a working search bar. For HTML websites.

## **How it works**
By clicking on the floating button the search bar is extended. As soon as the user enters text, `keyup` starts the `search()` function, which uses `filter` to hide all `<article>` elements that do not contain the search query. All other articles remain.


![Demo GIF](https://codeberg.org/pixelcode/SimpleSearchButton/raw/commit/9d48ff76c025cb205866cd7aa7a5cf3dabc275f0/SimpleSearchButton.gif)



**Clicking on the floating button toggles the search bar:**

```
$('#searchbuttondiv').click(function(){
	var searchfield = document.getElementById('searchfield').style.display;
	document.getElementById('searchfield').value = '';
	search();
	$('#searchfield').animate({ 'width': 'toggle' }, 300, 'swing');
	var html = $('#searchbutton').html();
    $('#searchbutton').html(
		html == '<i class="fas fa-search"></i>' ? '<a style="font-size: 25px"><i class="fas fa-times"></i></a>' : '<i class="fas fa-search"></i>');
});
```

**Typing into the search bar starts the search, so that the results are refreshed with each character. That's live search!**

```
$('#searchfield').keyup(function(){
	search();
});
```
**That's the actual `search()` function:**

```
function search(){
	var searchquery = document.getElementById('searchfield').value.toLowerCase();
	$('article').css({ 'display': 'block' });
	$('article').filter(function () {
		return ($(this).find('*').text().toLowerCase().indexOf(searchquery) == -1)
	}).css({ 'display': 'none' });
}
```
Any `article` that does not have a child element containing the search query will be kicked out.

## **I want to search other elements**
No problem, just change all occurrences of `article` in the `script.js` file to your element type or CSS class.

For example, if your element type is `p` then change all `article` occurrences to `p`, e.g.:

| change                                      | to                                    |
| ------------------------------------------- | ------------------------------------- |
| `$('article').css({ 'display': 'block' });` | `$('p').css({ 'display': 'block' });` |



## **License**
SimpleSearchButton may be used according to the [MIT licence](https://codeberg.org/pixelcode/SimpleSearchButton/src/branch/master/LICENCE.md) (slightly modified). A link to this repo as the original source would be great 😎


-----


**jQuery**: SimpleSearchButton uses [jQuery](https://jquery.com).

**Font Awesome**: SimpleSearchButton uses [Font Awesome](https://fontawesome.com) icons. These may be used under the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0) license.